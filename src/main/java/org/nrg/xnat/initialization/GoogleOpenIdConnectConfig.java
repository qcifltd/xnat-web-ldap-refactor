package org.nrg.xnat.initialization;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

@Configuration
@EnableOAuth2Client
public class GoogleOpenIdConnectConfig {
    @Value("${google.clientId}")
    private String clientId;

    @Value("${google.clientSecret}")
    private String clientSecret;

    @Value("${google.accessTokenUri}")
    private String accessTokenUri;

    @Value("${google.userAuthorizationUri}")
    private String userAuthorizationUri;

    @Value("${google.redirectUri}")
    private String redirectUri;

    @Bean
    public OAuth2ProtectedResourceDetails googleOpenId() {
    	System.out.println("Creating GoogleOPenIDCOnnectConfig....");
        final AuthorizationCodeResourceDetails details = new AuthorizationCodeResourceDetails();
//        details.setClientId(clientId);
//        details.setClientSecret(clientSecret);
//        details.setAccessTokenUri(accessTokenUri);
//        details.setUserAuthorizationUri(userAuthorizationUri);
        details.setClientId("743472296560-0ivhmlki5p2773lih27enac9av5ro4a3.apps.googleusercontent.com");
        details.setClientSecret("CV2xWbJTOXcxB5hQ9BoNOR4i");
        details.setAccessTokenUri("https://www.googleapis.com/oauth2/v3/token");
        details.setUserAuthorizationUri("https://accounts.google.com/o/oauth2/auth");
        
        details.setScope(Arrays.asList("openid", "email"));
        details.setPreEstablishedRedirectUri("http://myxnatlocal.com/app/template/google-login");
        details.setUseCurrentUri(false);
        return details;
    }

    @Bean
    public OAuth2RestTemplate googleOpenIdTemplate(final OAuth2ClientContext clientContext) {
        final OAuth2RestTemplate template = new OAuth2RestTemplate(googleOpenId(), clientContext);
        return template;
    }

}
